## Conversion calculator
It is a currency converter application, built in Java using Swing. It uses the exchange rates provided by the European Central Bank ([here](https://exchangeratesapi.io/) is the link).
In order to skip compiling the calculator, you can simply download the JAR file and run it.