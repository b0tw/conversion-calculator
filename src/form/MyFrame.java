package form;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.json.JSONObject;



public class MyFrame implements ActionListener {

	private JFrame frame;
	private Container container;
	private JButton convertButton;
	private JLabel initialCurrencyLabel;
	private JLabel desiredCurrencyLabel;
	private JLabel message;
	private JTextField initialCurrencyField;
	private JTextField desiredCurrencyField;
	private JComboBox<String> initialCurrencyBox;
	private JComboBox<String> desiredCurrencyBox;
	private String[] currencies = { "RON", "EUR", "USD", "GBP" };

	MyFrame() {
		frame = new JFrame("Conversion calculator");
		frame.setBounds(300, 90, 400, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);

		container = frame.getContentPane();
		container.setLayout(null);

		initialCurrencyLabel = new JLabel("Initial currency");
		initialCurrencyLabel.setSize(200, 50);
		initialCurrencyLabel.setLocation(100, 50);
		container.add(initialCurrencyLabel);

		initialCurrencyField = new JTextField();
		initialCurrencyField.setSize(200, 30);
		initialCurrencyField.setLocation(100, 100);
		container.add(initialCurrencyField);

		initialCurrencyBox = new JComboBox<String>(currencies);
		initialCurrencyBox.setSize(100, 30);
		initialCurrencyBox.setLocation(100, 150);
		container.add(initialCurrencyBox);

		desiredCurrencyLabel = new JLabel("Desired currency");
		desiredCurrencyLabel.setSize(200, 50);
		desiredCurrencyLabel.setLocation(100, 200);
		container.add(desiredCurrencyLabel);

		desiredCurrencyField = new JTextField();
		desiredCurrencyField.setSize(200, 30);
		desiredCurrencyField.setLocation(100, 250);
		desiredCurrencyField.setEditable(false);
		container.add(desiredCurrencyField);

		desiredCurrencyBox = new JComboBox<String>(currencies);
		desiredCurrencyBox.setSize(100, 30);
		desiredCurrencyBox.setLocation(100, 300);
		container.add(desiredCurrencyBox);

		convertButton = new JButton("Convert");
		convertButton.setSize(100, 20);
		convertButton.setLocation(100, 350);
		convertButton.addActionListener(this);
		container.add(convertButton);

		message = new JLabel();
		message.setSize(200, 50);
		message.setLocation(100, 370);
		message.setVisible(false);
		container.add(message);

		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == convertButton) {
			message.setText("");
			message.setVisible(false);

			String initialc = initialCurrencyField.getText();

			double initialCurrency = 0;
			try {
				initialCurrency = Double.parseDouble(initialc);
			} catch (Exception ex) {
				message.setText("Please input a number");
				message.setVisible(true);
			}
			String base = (String) initialCurrencyBox.getSelectedItem();
			String symbol = (String) desiredCurrencyBox.getSelectedItem();
			String api = String.format("https://api.exchangeratesapi.io/latest?base=%s&symbols=%s", base, symbol);

			try {
				URL url = new URL(api);
				URLConnection connection = url.openConnection();
				InputStream inputStream = connection.getInputStream();
				InputStreamReader reader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(reader);

				StringBuilder stringBuilder = new StringBuilder();
				String line = null;
				while ((line = bufferedReader.readLine()) != null) {
					stringBuilder.append(line);
				}
				String jsonText = stringBuilder.toString();
				System.out.println(jsonText);
				JSONObject jsonObject = new JSONObject(jsonText);
				JSONObject rateObject = jsonObject.getJSONObject("rates");
				double rate = rateObject.getDouble(symbol);
				double desiredCurrency = initialCurrency * rate;
				desiredCurrencyField.setText(Double.toString(desiredCurrency));
			} catch (IOException e1) {
				message.setText("Problem with the api. Try again later.");
				message.setVisible(true);
			} catch(Exception e2) {
				message.setText("Something else happened");
				message.setVisible(true);
			}

		}

	}

}
